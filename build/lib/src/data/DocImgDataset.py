# -*- coding: utf-8 -*-
import os
import random
import cv2
from typing import Union

import numpy as np
import torch
import albumentations as A
from torch.utils.data import Dataset
from albumentations.augmentations.geometric.resize import Resize
from PIL import Image, ImageFile

ImageFile.LOAD_TRUNCATED_IMAGES = True


class DocImgDataset(Dataset):
    def __init__(
        self,
        df: object,
        path: Union[None, dict[str, str]] = {
            "flick": "data/raw/flickr8k/images",
            "tobacoo": "data/raw/Tobacco 800 Dataset/tobacco800",
            "pub_lay_net": "data/raw/PubLayNet/train-0/publaynet/train",
        },
        normalize: Union[None, tuple[float]] = (178.4238, 7237.1367),
        transformer: Union[None, object] = A.Compose(
            [
                A.Rotate(limit=80, p=0.9, border_mode=cv2.BORDER_CONSTANT),
                A.HorizontalFlip(p=0.6),
                A.VerticalFlip(p=0.5),
            ]
        ),
        tr_chance: float = 0.6,
        image_size: tuple[int] = (720, 720),
        inversion: Union[None, list[str]] = ["tobacoo"],
        inv_chance: float = 0.8,
        return_orig: bool = False,
        root_project_path: Union[None, str] = None,
    ) -> dict[str, object]:
        super(DocImgDataset, self).__init__()
        """
        Parameters:
            df (object): dataset with columns: ['file_name', 'target', 'dataset']
            use make_df() for this path (dict) dict where key dataset name
            from column 'dataset' (result make_df()). Path relative to the project root folder
            normalize (tuple(float, float)): mean and standard deviation for normalization,
            if False, does not apply
            transformer (False, object): class from albumentations for transform images,
            if False, does not apply
            tr_chance: float: chance use transformer for images
            image_size (tuple(int, int)): height and width for for init Resiz from
            albumentations for resiz images
            inversion (False, list[str]): list with dataset name from column 'dataset'
            (result make_df()), in which the white color will change to the black color
            and vice versa. Tobacoo dataset have document with black background and in
            white letters.
            inv_chance (float): chance use inversion for images
            return_orig (bool): if True return tensor original images without normalize,
            transformer, inversion
            root_project_path (None, str): absolute path to the project root folder

        Result:
            dict {
                "img_x": tensor for model,
                "index": index from dataset,
                "target": tonsor with target,
                "dataset_index": this index allows to find out the name of the original
                 dataset for the current example (use attribute dataset_index)
                "img_orig": tenosor image without normalize, transformer, inversion.
                        This key retern if parametr return_orig True
                }
        """
        print("absolute path to the project root folder:", root_project_path)
        if root_project_path is not None:
            for dataset_name, path_data in path.items():
                path[dataset_name] = os.path.join(root_project_path, path_data)

        self.df = df
        self.path = path
        if image_size:
            self.resize_image = Resize(image_size[0], image_size[1])

        self.transformer = transformer
        self.normalize = normalize
        self.tr_chance = tr_chance
        self.inversion = inversion
        self.inv_chance = inv_chance
        self.return_orig = return_orig
        self.dataset_index = {
            name: num for num, name in enumerate(df["dataset"].unique())
        }

    def invers_doc(self, img_doc):
        img_buffer = img_doc.copy()
        img_doc[img_buffer == 0], img_doc[img_buffer == 255] = 255, 0
        return img_doc

    def __len__(self):
        return len(self.df)

    def __getitem__(self, index):
        file_name = self.df.loc[index, "file_name"]
        target = self.df.loc[index, "target"]
        dataset_name = self.df.loc[index, "dataset"]
        path = self.path[dataset_name]

        img_x = Image.open(f"{path}/{file_name}")
        img_x = np.array(img_x.convert("L"), dtype="float32")[:, :, None]

        if self.return_orig:
            img_orig = img_x.copy()

        if self.resize_image:
            img_x = (
                self.resize_image(image=img_x)["image"] if self.resize_image else img_x
            )

        if dataset_name in self.inversion and (random.uniform(0, 1) < self.inv_chance):
            img_x = self.invers_doc(img_x)

        if self.normalize:
            img_x = (img_x - self.normalize[0]) / self.normalize[1]

        if self.transformer and (random.uniform(0, 1) < self.tr_chance):
            img_x = self.transformer(image=img_x)["image"]

        img_x = torch.from_numpy(img_x).permute(2, 0, 1)

        dict_result = {
            "img_x": img_x,
            "index": index,
            "target": np.float32(target),
            "dataset_index": self.dataset_index[dataset_name],
        }

        if self.return_orig:
            dict_result["img_orig"] = img_orig

        return dict_result
