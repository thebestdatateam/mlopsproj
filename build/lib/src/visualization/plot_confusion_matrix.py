import os
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.metrics import confusion_matrix


def plot_confusion_matrix(
    X: list[int] = [],
    y: list[int] = [],
    size: tuple[int, int] = (10, 8),
    mode: str = "save",
    path_save: str = "reports/model_clf_conf_martix.png",
    root_project_path: str = None,
) -> object:
    """
    draws a confusion matrix or save it

    Parameters:
        X (list): list with predictions. Only 1 or 0
        y (list): list with ground truth, Only 1 or 0
        size (tuple): plot size
        mode (str): if show - print plot if mode 'save' save plot like file
        root_project_path (str): path for root project folder
    Return:
        plot with confusion matrix

    """

    matrix = confusion_matrix(X, y)
    plt.figure(figsize=size)
    classes = [
        "True Negative",
        "False Positive",
        "False Negative",
        "True Positive",
    ]
    values = ["{0:0.0f}".format(x) for x in matrix.flatten()]
    percentages = ["{0:.1%}".format(x) for x in matrix.flatten() / np.sum(matrix)]
    combined = [f"{i}\n{j}\n{k}" for i, j, k in zip(classes, values, percentages)]
    combined = np.asarray(combined).reshape(2, 2)
    plot = sns.heatmap(matrix, annot=combined, fmt="", cmap="YlGnBu")
    plot.set(title="Confusion Matrix")
    plot.set(xlabel="Predicted", ylabel="Actual")
    if mode == "save":
        if root_project_path is not None:
            path_save = os.path.join(root_project_path, path_save)
        plt.savefig(path_save)

    elif mode == "show":
        plt.show()
