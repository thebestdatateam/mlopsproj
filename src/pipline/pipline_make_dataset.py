import click

from src import make_dataset


@click.command()
@click.option("--name_df", type=list, default=["tobacoo", "flick", "pub_lay_net"])
@click.option(
    "--df_dict",
    type=dict,
    default={
        "flick": ["data/raw/flickr8k/images", 0],
        "tobacoo": ["data/raw/Tobacco 800 Dataset/tobacco800", 1],
        "pub_lay_net": ["data/raw/PubLayNet/train-0/publaynet/train", 1],
    },
)
@click.option(
    "--size_data",
    type=dict,
    default={"flick": 10000, "tobacoo": 1500, "pub_lay_net": 8500},
)
@click.option("--train_size", type=float, default=0.8)
# pathes relative to the project root folder to the folder with the outgoing data
@click.option("--path_out_train", type=click.Path(), default="data/interim/train.csv")
@click.option("--path_out_valid", type=click.Path(), default="data/interim/val.csv")
@click.option("--path_out_test", type=click.Path(), default="data/interim/test.csv")
def main(
    name_df,
    df_dict,
    size_data,
    train_size,
    path_out_train,
    path_out_valid,
    path_out_test,
):
    train, val, test = make_dataset(
        name_df=name_df, df_dict=df_dict, size_data=size_data, train_size=train_size
    )

    train.to_csv(path_out_train)
    val.to_csv(path_out_valid)
    test.to_csv(path_out_test)


if __name__ == "__main__":
    main()
