import os
import json

import click
import torch
import pandas as pd
import torch.nn.functional as F
from torch.utils.data import DataLoader

from src import ModelLight, DocImgDataset


@click.command()
@click.option("--model_path", type=click.Path(), default="models/model_clf.ckpt")
@click.option("--path_valid", type=click.Path(), default="data/interim/val.csv")
@click.option("--path_test", type=click.Path(), default="data/interim/test.csv")
@click.option("--devices", default=None)
@click.option("--batch_size", type=int, default=10)
@click.option(
    "--output_file", type=click.Path(), default="data/interim/text_images.json"
)
def main(model_path, path_valid, path_test, devices, batch_size, output_file):
    df_val = pd.read_csv(path_valid)
    df_test = pd.read_csv(path_test)

    df = pd.concat([df_test, df_val])

    df = df[df["dataset"] != "pub_lay_net"]

    df.reset_index(drop=True, inplace=True)
    data = DocImgDataset(df, transformer=None, inv_chance=0)
    data_loader = DataLoader(data, batch_size=batch_size)

    model = ModelLight.load_from_checkpoint(model_path, map_location="cpu")
    model.eval()

    if devices is not None:
        model.to(f"cuda:{devices}")

    list_pred = []
    with torch.no_grad():
        for batch in data_loader:
            img_x = batch["img_x"]
            if devices is not None:
                img_x = img_x.to(f"cuda:{devices}")

            prediction = F.sigmoid(model.forward(img_x).squeeze_()).cpu().tolist()

            # if batch size 1 F.sigmoid return float
            if type(prediction) == float:
                list_pred.append(prediction)
            else:
                list_pred.extend(prediction)

    result_dict = {}

    for pred, name, name_dataset in zip(
        list_pred, df["file_name"].to_list(), df["dataset"].to_list()
    ):
        if pred == 1:
            result_dict[name] = {
                "load_path": os.path.join(data.path[name_dataset], name),
                "extract_text": None,
            }

    with open(output_file, "w") as fp:
        json.dump(result_dict, fp)


if __name__ == "__main__":
    main()
