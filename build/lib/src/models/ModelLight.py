import torch
import torch.nn.functional as F
import lightning.pytorch as pl
from lightning.pytorch import seed_everything
from pytorch_optimizer import AdaBelief
from torchmetrics.functional.classification import (
    binary_auroc,
    binary_accuracy,
    binary_recall,
    binary_precision,
    binary_f1_score,
)

seed_everything(42, workers=True)


class ModelLight(pl.LightningModule):
    def __init__(self, model, lr=0.003):
        super().__init__()
        self.save_hyperparameters()
        self.lr = lr
        self.model = model
        self.example_input_array = torch.rand(1, 1, 720, 720)

        self.predict_list = []
        self.target_list = []
        self.test_predict_list = torch.rand(0)
        self.test_target_list = torch.rand(0)

        self.test_metric = None

    def forward(self, x):
        return self.model(x)

    def configure_optimizers(self):
        optimizer = AdaBelief(self.model.parameters(), lr=self.lr)
        return optimizer

    def training_step(self, batch, batch_idx):
        # training_step defines the train loop.
        x = batch["img_x"]
        y = batch["target"]
        pred = torch.squeeze(self(x))
        loss = F.binary_cross_entropy_with_logits(pred, y)
        self.log("train_loss", loss, prog_bar=False, batch_size=x.shape[0])

        return loss

    def validation_step(self, batch, batch_idx):
        self._shared_eval_step(batch, batch_idx)

    def on_validation_epoch_end(self):
        (
            loss,
            auc,
            accuracy,
            precision,
            recall,
            f1_score,
        ) = self._shared_eval_epoch()

        mectrics = {
            "val_loss": loss,
            "val_auc": auc,
            "val_accuracy": accuracy,
            "val_precision": precision,
            "val_recall": recall,
            "val_f1_score": f1_score,
        }

        self.log_dict(mectrics)

    def test_step(self, batch, batch_idx):
        self._shared_eval_step(batch, batch_idx)

    def on_test_end(self):
        (
            _,
            auc,
            accuracy,
            precision,
            recall,
            f1_score,
        ) = self._shared_eval_epoch(save_predict=True)
        mectrics = {
            "test_auc": auc,
            "test_accuracy": accuracy,
            "test_precision": precision,
            "test_recall": recall,
            "test_f1_score": f1_score,
        }
        self.test_metric = mectrics

    def _shared_eval_step(self, batch, batch_idx):
        with torch.no_grad():
            x = batch["img_x"]
            y = batch["target"].cpu()
            prediction = F.sigmoid(self(x).squeeze_()).cpu()
            self.predict_list.append(prediction)
            self.target_list.append(y)

    def _shared_eval_epoch(self, save_predict=False):
        with torch.no_grad():
            self.predict_list = torch.cat(self.predict_list)
            self.target_list = torch.cat(self.target_list)
            loss = F.binary_cross_entropy_with_logits(
                self.predict_list, self.target_list
            )

            self.target_list = self.target_list.long()
            auc = binary_auroc(self.predict_list, self.target_list)

            self.predict_list = self.predict_list.round_().long()

            accuracy = binary_accuracy(self.predict_list, self.target_list)
            precision = binary_precision(self.predict_list, self.target_list)
            recall = binary_recall(self.predict_list, self.target_list)
            f1_score = binary_f1_score(self.predict_list, self.target_list)

            if save_predict:
                self.test_predict_list, self.test_target_list = (
                    self.predict_list.clone().detach(),
                    self.target_list.clone().detach(),
                )

            self.predict_list = []
            self.target_list = []

        return loss, auc, accuracy, precision, recall, f1_score
