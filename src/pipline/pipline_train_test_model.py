import click
import torch
import lightning.pytorch as pl
from lightning.pytorch.callbacks import ModelCheckpoint, LearningRateMonitor
from lightning.pytorch.tuner import Tuner
from lightning.pytorch import seed_everything

from src import resnet18, ModelLight, LitData, plot_confusion_matrix

seed_everything(42, workers=True)


@click.command()
@click.option("--train", type=click.Path(), default="data/interim/train.csv")
@click.option("--valid", type=click.Path(), default="data/interim/val.csv")
@click.option("--test", type=click.Path(), default="data/interim/test.csv")
@click.option("--batch_size", type=int, default=20)
@click.option("--num_workers", type=int, default=2)
@click.option("--normalize", type=(float, float), default=(178.4238, 7237.1367))
@click.option("--accelerator", type=str, default="auto")
@click.option("--devices", default="auto")
@click.option("--log", is_flag=True)
@click.option("--model_path_save", type=click.Path(), default="models/model_clf.ckpt")
@click.option("--max_epoch", type=int, default=10)
@click.option(
    "--plot_path_save",
    type=click.Path(),
    default="reports/figures/model_clf_conf_martix.png",
)
def main(
    train,
    valid,
    test,
    batch_size,
    num_workers,
    normalize,
    accelerator,
    devices,
    log,
    model_path_save,
    max_epoch,
    plot_path_save,
):
    devices = [int(devices)]

    datamodule = LitData(
        train,
        valid,
        test,
        batch_size=batch_size,
        num_workers=num_workers,
        normalize=normalize,
    )

    checkpoint_callback = ModelCheckpoint(
        monitor="val_loss", dirpath="checkpoint", filename="checkpoint", mode="min"
    )

    if log:
        lr_logger = LearningRateMonitor(logging_interval="epoch")
        callbacks = [checkpoint_callback, lr_logger]
    else:
        callbacks = [checkpoint_callback]

    model = ModelLight(resnet18(1, 1))
    trainer = pl.Trainer(
        callbacks=callbacks,
        accelerator=accelerator,
        devices=devices,
        max_epochs=max_epoch,
        logger=log,
        deterministic=True,
    )

    compiled_model = torch.compile(model)

    tuner = Tuner(trainer)

    tuner.scale_batch_size(compiled_model, datamodule=datamodule, mode="binsearch")
    # if use batch size that find from tuner.scale_batch_size will memory error
    datamodule.batch_size -= 3

    tuner.lr_find(compiled_model, datamodule=datamodule)

    trainer.fit(compiled_model, datamodule=datamodule)
    trainer.save_checkpoint(model_path_save)

    trainer.test(model, datamodule=datamodule)
    print(model.test_metric)

    X, y = model.test_predict_list, model.test_target_list
    plot_confusion_matrix(X, y, mode="save", path_save=plot_path_save)


if __name__ == "__main__":
    main()
