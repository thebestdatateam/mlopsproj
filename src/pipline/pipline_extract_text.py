import json
import click
from src import convert_img_to_text

@click.command()
@click.option("--load_path", type=click.Path(), default="data/interim/text_images.json")
def main(load_path):
    with open(load_path) as json_file:
        data = json.load(json_file)
        for key, value in data.items():
            text = convert_img_to_text(value["load_path"])
            if text:
                value["extract_text"] = text

    with open(load_path, "w") as json_file:
        json.dump(data, json_file)

if __name__ == '__main__':
    main()