from typing import Pattern

import pytesseract
import numpy as np
import re
from lemmagen3 import Lemmatizer
from stop_words import get_stop_words
from operator import itemgetter


def convert_img_to_text(img_path: str) -> str:
    text = pytesseract.image_to_string(img_path, lang="eng")
    return text


def normalizer(text: str) -> list[str]:
    lem_en = Lemmatizer("en")
    regex_num: Pattern[str] = re.compile('([\d])[\s]+([\d])')
    regex_punct = re.compile(
        "[%s]" % re.escape("!\"#$%&'()*+,./:;<=>?@[\\]^`{|}~«»‘•©№…")
    )
    stopwords_list = get_stop_words("en")

    text = regex_punct.sub(" ", text)
    text = regex_num.sub("\\1\\2", text)
    text = text.lower()
    terms = []

    for word in text.split():
        word = lem_en.lemmatize(word)
        if word not in stopwords_list:
            terms.append(word)
    return terms


def bag_of_words(doc: str) -> dict[str, int]:
    words_vector = {}
    for word in doc:
        words_vector[word] = words_vector.setdefault(word, 0) + 1
    return words_vector


def length_vec(vec: dict) -> float:
    return np.linalg.norm(list(vec.values()))


def normalize_vec(vec: dict) -> dict[str, float]:
    len_vec = length_vec(vec)
    if len_vec == 0:
        return 0
    norm_vec = dict(zip(list(vec.keys()), list(vec.values()) / len_vec))
    return norm_vec


def dot_vec(v1: dict, v2: dict) -> float:
    intersection = set.intersection(set(v1.keys()), set(v2.keys()))
    value = 0
    for key in intersection:
        value += v1[key] * v2[key]
    return value


def vector_ranking(query: dict, docs: list[dict], top_n: int = 3) -> list:
    rang = []
    for doc in docs:
        rang.append(dot_vec(query, doc) / (length_vec(query) * length_vec(doc)))
    return sorted(zip(range(len(rang)), rang), key=itemgetter(1), reverse=True)[:top_n]
