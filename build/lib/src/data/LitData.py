from typing import Union

import pandas as pd
import lightning as L
from .DocImgDataset import DocImgDataset
from torch.utils.data import DataLoader


class LitData(L.LightningDataModule):
    def __init__(
        self,
        train_path: str = None,
        valid_path: str = None,
        test_path: str = None,
        batch_size: int = 20,
        num_workers: int = 4,
        normalize: Union[None, tuple[float, float]] = (178.4238, 7237.1367),
    ):
        """
        Parameters:
            train_path (str): path for with csv with train dataset
            valid_path (str): path for with csv with valid dataset
            test_path (str): path for with csv with test dataset
            batch_size (int): bach size for DataLoader
            num_workers (int) num_workers for DataLoader
        """
        super().__init__()

        self.train_path = train_path
        self.valid_path = valid_path
        self.test_path = test_path
        self.batch_size = batch_size
        self.num_workers = num_workers
        self.normalize = normalize

    def prepare_data(self):
        # download
        self.train_csv = pd.read_csv(self.train_path)
        self.valid_csv = pd.read_csv(self.valid_path)
        self.test_csv = pd.read_csv(self.test_path)

    def setup(self, stage=None):
        # Assign train/val datasets for use in dataloaders
        if stage in "fit" or stage is None:
            self.train_data = DocImgDataset(self.train_csv, normalize=self.normalize)

            self.valid_data = DocImgDataset(
                self.valid_csv, normalize=self.normalize, transformer=None
            )
        if stage == "test":
            self.test_data = DocImgDataset(
                self.test_csv, normalize=self.normalize, transformer=None
            )

    def train_dataloader(self):
        return DataLoader(
            self.train_data, batch_size=self.batch_size, num_workers=self.num_workers
        )

    def val_dataloader(self):
        return DataLoader(
            self.valid_data, batch_size=self.batch_size, num_workers=self.num_workers
        )

    def test_dataloader(self):
        return DataLoader(
            self.test_data, batch_size=self.batch_size, num_workers=self.num_workers
        )
