import shutil
import os
import json
from os import listdir
from datetime import date
from typing import Union

import numpy as np
import pandas as pd
import torch
import torch.nn.functional as F
from torch.utils.data import Dataset
from torch.utils.data import DataLoader

from PIL import Image
from albumentations.augmentations.geometric.resize import Resize

from ..models import ModelLight


class DataImage(Dataset):
    def __init__(
        self,
        dir_input: str,
        normalize: Union[None, tuple[float]] = (178.6541, 7216.7637),
        image_size: tuple[int] = (720, 720),
    ):
        """
        The class preprocesses images

        Parameters:
        dir_input (str): path for images
        normalize (Union[None, tuple[float]]): mean and std for normalize images,
        if None does not apply
        image_size (tuple[int]): sizes for resize images
        """

        super().__init__()
        self.dir_input = dir_input
        self.normalize = normalize
        self.not_images = []
        self.images = []

        for name in listdir(self.dir_input):
            if name.endswith(".png") or name.endswith(".jpg"):
                self.images.append(name)
            else:
                self.not_images.append(name)

        self.resize_image = Resize(image_size[0], image_size[1])

    def __len__(self):
        return len(self.images)

    def __getitem__(self, index):
        file_name = self.images[index]

        img_x = Image.open(f"{self.dir_input}/{file_name}")
        img_x = np.array(img_x.convert("L"), dtype="float32")[:, :, None]

        img_x = self.resize_image(image=img_x)["image"]

        if self.normalize:
            img_x = (img_x - self.normalize[0]) / self.normalize[1]

        img_x = torch.from_numpy(img_x).permute(2, 0, 1)

        return {"img_x": img_x, "index_name": index}


class ImageClf:
    def __init__(
        self,
        model: str = "models/model_clf.ckpt",
        dir_input: str = "images/input",
        output_file: str = "images/result.json",
        border: float = 0.5,
        batch_size: int = 1,
        num_workers: int = 1,
        device: str = None,
        make_raport: bool = True,
        raport_folder: str = "images/raports",
        raport_type: str = "xlsx",
        normalize: tuple = (178.6541, 7216.7637),
        sort_in_dir: bool = False,
        dir_text: str = "images/text",
        dir_not_text: str = "images/not_text",
        root_project_path: Union[None, str] = None,
    ):
        """
        The class determines the type of file (text or not) and sorts them

        Parameters:
        model (str): path for model
        dir_input (str): relative path to the folder with the files to classify
        output_file (str): name for json file with result
        border (float): border for image classification
        return_not_image (bool): if True make folder for images that not text
        batch_size (int): size for batch for model (DataLoader)
        num_workers (int): number Number of processors used for image preparation
        (DataLoader)
        device (str): number gpu if None will use cpu
        make_raport(bool): if Ture make raport
        raport_folder(str): relative path for raports
        raport_type: raport can be excel_table (xlsx) or csv-file (csv)
        normalize (tuple): mean and std for normalize images
        sort_in_dir (bool): if True sort images in folders (dir_text, dir_not_text)
        dir_text (str): path for folder for text images
        dir_not_text (str): path for folder for non text images
        root_project_path (None, str): absolute path to the project root folder
        if use code form not project root folder use this parameter
        """

        if root_project_path is not None:
            self.model_path = os.path.join(root_project_path, model)
            self.dir_input = os.path.join(root_project_path, dir_input)
            self.output_file = os.path.join(root_project_path, output_file)
            self.dir_text = os.path.join(root_project_path, dir_text)
            self.dir_not_text = os.path.join(root_project_path, dir_not_text)
            self.raport_folder = os.path.join(root_project_path, raport_folder)
        else:
            self.model_path = model
            self.dir_input = dir_input
            self.output_file = output_file
            self.dir_text = dir_text
            self.dir_not_text = dir_not_text
            self.raport_folder = raport_folder

        self.border = border
        self.batch_size = batch_size
        self.num_workers = num_workers
        self.device = device
        self.make_raport = make_raport
        self.raport_type = raport_type
        self.sort_in_dir = sort_in_dir

        self.model = ModelLight.load_from_checkpoint(
            self.model_path, map_location="cpu"
        )

        data = DataImage(dir_input=dir_input, normalize=normalize)
        self.images = data.images
        self.not_images = data.not_images

        self.data_loarder = DataLoader(
            data, batch_size=batch_size, num_workers=num_workers
        )

        self.result_dict = {
            "name_file": [],
            "target": [],
            "path": [],
            "probability": [],
        }
        self.text_images = []
        self.not_text_images = []
        self.probability_text = []
        self.probability_not_text = []

        if make_raport:
            self.__create_folder(raport_folder, remove_folder=False)

        if sort_in_dir:
            self.__create_folder(dir_text)
            self.__create_folder(dir_not_text)

        if self.device is not None:
            self.model.to(f"cuda:{device}")
        self.model.eval()

    def __create_folder(self, relative_path, remove_folder=True):
        "Method remove and create folder"
        absolute_path = f"{os.getcwd()}/{relative_path}"
        if remove_folder:
            shutil.rmtree(absolute_path, ignore_errors=True)
            os.mkdir(absolute_path)
        else:
            if not os.path.exists(absolute_path):
                os.makedirs(absolute_path)

    def __classify(self):
        "The method defines the class of the image, its probability"
        with torch.no_grad():
            for batch in self.data_loarder:
                img_x = batch["img_x"]
                if self.device is not None:
                    img_x = img_x.to(f"cuda:{self.device}")

                index_name = batch["index_name"].long().tolist()

                prediction = (
                    F.sigmoid(self.model.forward(img_x).squeeze_()).cpu().tolist()
                )

                # if batch size 1 F.sigmoid return float
                if type(prediction) == float:
                    prediction = [prediction]

                probability = prediction
                prediction = [1 if i >= self.border else 0 for i in prediction]

                file_name = [self.images[index] for index in index_name]
                path = [
                    os.path.abspath(f"{self.dir_input}/{name}") for name in file_name
                ]

                self.result_dict["name_file"].extend(file_name)
                self.result_dict["target"].extend(prediction)
                self.result_dict["path"].extend(path)
                self.result_dict["probability"].extend(probability)

    def __make_json(self):
        "This method make json-file with file-name, absolute path, target"
        with open(self.output_file, "w") as fp:
            json.dump(self.result_dict, fp)

    def __make_raport(self):
        "The method prepares a report"

        self.df_raport = pd.DataFrame(self.result_dict)

    def __save_raport(self):
        current_date = date.today()
        version = ""
        files_list = [name.split(".")[0] for name in listdir(self.raport_folder)]

        contin = True
        num = 0
        while contin:
            raport_name = f"raport_{current_date}{version}"
            if raport_name not in files_list or num >= 100:
                contin = False
                if self.raport_type == "xlsx":
                    self.df_raport.to_excel(f"{self.raport_folder}/{raport_name}.xlsx")
                elif self.raport_type == "csv":
                    self.df_raport.to_csv(f"{self.raport_folder}/{raport_name}.csv")
                else:
                    print("Use for raport xlsx or csv! Raport save like file excel!")
                    self.df_raport.to_excel(f"{self.raport_folder}/{raport_name}.xlsx")
            else:
                num += 1
                version = f"_{num}"

    def __sort_images(self):
        "The method sorts the images into folders"
        for name, target in zip(
            self.result_dict["name_file"], self.result_dict["target"]
        ):
            if target == 1:
                src_file = f"{self.dir_input}/{name}"
                dest_file = f"{self.dir_text}/{name}"
                shutil.copy2(src_file, dest_file, follow_symlinks=True)

            else:
                src_file = f"{self.dir_input}/{name}"
                dest_file = f"{self.dir_not_text}/{name}"
                shutil.copy2(src_file, dest_file, follow_symlinks=True)

    def run(self):
        self.__classify()
        self.__make_json()

        if self.make_raport:
            self.__make_raport()
            self.__save_raport()

        if self.sort_in_dir:
            self.__sort_images()

        num_text = self.result_dict["target"].count(1)
        num_not_text = self.result_dict["target"].count(0)
        not_img = len(self.not_images)
        total_image = num_text + num_not_text + not_img

        print(f"Total files: {total_image}")
        print(f"Number of text image files: {num_text}")
        print(f"Number of non-text image files: {num_not_text}")
        print(f"Number of non-image files: {not_img}")
