# -*- coding: utf-8 -*-
from typing import Union

import torch


def get_param_for_normalize(
    trainset: object, num_cn: int = 1
) -> tuple[Union[int, float]]:
    """
    Method for calculating normalization parameters (use DataLoader from pytorch)

    Parameters:
        trainset (object): tensor with dimension (batch, channel, height, width)
        num_cn (int): number of image channels

    Result:
        tuple(int, float, float): return number cells, mean and std for every channel
    """

    mu = torch.zeros((num_cn,), dtype=torch.float)
    sig = torch.zeros((num_cn,), dtype=torch.float)
    n = 0
    with torch.no_grad():
        for data in trainset:
            imgs = data["img_x"]
            mu += torch.sum(imgs, dim=(0, 2, 3))
            sig += torch.sum(imgs**2, dim=(0, 2, 3))
            n += imgs.numel()  # // imgs.shape[0]
    n = float(n) / num_cn
    mu = mu / n  # mean
    sig = sig / n - (mu**2)  # std
    return n, mu, sig
