# -*- coding: utf-8 -*-
import os
from random import shuffle
from os import listdir
from typing import Union

import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split

np.random.seed(1)


def make_dataset(
    name_df: list[str] = ["tobacoo", "flick", "pub_lay_net"],
    df_dict: dict[str, list[str, int]] = {
        "flick": ["data/raw/flickr8k/images", 0],
        "tobacoo": ["data/raw/Tobacco 800 Dataset/tobacco800", 1],
        "pub_lay_net": ["data/raw/PubLayNet/train-0/publaynet/train", 1],
    },
    size_data: dict = {"flick": 10000, "tobacoo": 1500, "pub_lay_net": 8500},
    train_size: float = 0.8,
    root_project_path: Union[None, str] = None,
) -> tuple[object]:
    """
    Method make dataset from any datasets

    Parameters:
        name_df (None, list[str]): names datasets that will be included in df
        df_dict (None, dict): dict, where keys are name datasets and values are
        path for theme
        size_data (int): the number of examples from each dataset. Use -1 for
        all exampels
        train_size (float): size train dataset
        root_project_path (None, str): absolute path to the project root folder

    Result:
        DataFrames for train, valid, test data. Data columns are as follows:
        ==========  ==============================================================
        index       index for dataset
        file_name   file name as in the original dataset
        target      1 or 0 (image with document or not)
        dataset     name original dataset
        ==========  ==============================================================

    """
    print("absolute path to the project root folder:", root_project_path)
    if root_project_path is not None:
        for dataset_name, path_data in df_dict.items():
            df_dict[dataset_name][0] = os.path.join(root_project_path, path_data[0])

    list_df = []

    for name in name_df:
        path = df_dict[name][0]
        target = df_dict[name][1]

        image_file = [
            f for f in listdir(path) if f.endswith(".png") or f.endswith(".jpg")
        ]

        shuffle(image_file)

        image_file = image_file[: size_data[name]]

        df_buffer = pd.DataFrame(image_file, columns=["file_name"])
        df_buffer["target"] = target
        df_buffer["dataset"] = name

        list_df.append(df_buffer)

    df = pd.concat(list_df)

    train, val = train_test_split(
        df, train_size=train_size, stratify=df["target"], random_state=1
    )
    test, val = train_test_split(
        val, test_size=0.5, stratify=val["target"], random_state=1
    )

    return (
        train.reset_index(drop=True),
        val.reset_index(drop=True),
        test.reset_index(drop=True),
    )
