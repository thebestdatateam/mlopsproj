import os


def path_reduction(num_reduction: int = 2) -> str:
    """
    The function returns the absolute path to the specified number of levels back

    Parameters:
        num_reduction (int): the number of steps backward on the absolute path

    Result:
        reduction_abcolut_path (str): absolute path to the specified number of levels back

    """
    abcolut_path = os.getcwd()
    for i in range(num_reduction):
        abcolut_path = os.path.dirname(abcolut_path)

    return abcolut_path
